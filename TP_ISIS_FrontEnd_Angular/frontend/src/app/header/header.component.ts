import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { WorldService } from '../Services/world.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  worldSub:Subscription;
  world:any;

  managers:any;
  cptManagers = 0

  upgrades:any;
  cptUpgrades = 0

  username:string;
  changeUsername : boolean = false;

  facteursMultiplicatifs = [1,10,100]
  currentFacteur = 0

  @Output() changeMenu = new EventEmitter<string>();

  constructor(private worldService:WorldService) { }

  ngOnInit() {
    if(localStorage.getItem("username")){
      this.username = localStorage.getItem("username")
    }else{
      this.worldService.username$.subscribe(
        (res)=>{
          this.username = res
        }
      )
    }
    this.worldSub = this.worldService.world$.subscribe(
      (res:any)=>{
        this.world = res;
        this.managers = res.managers
        this.upgrades = res.upgrades
        this.cptManagers = 0
        this.cptUpgrades = 0;
        this.countAvailableManagers()
        this.countAvailableUpgrades()
      }
    )
  }

  countAvailableManagers(){
    this.managers.pallier.forEach(element => {
      if(this.world.money>=element.seuil && element.unlocked==false){
        this.cptManagers+=1
      }
    });
  }

  countAvailableUpgrades(){
    this.upgrades.pallier.forEach(element => {
      if(this.world.money>=element.seuil && element.unlocked==false){
        this.cptUpgrades+=1
      }
    });
  }

  changeFacteur(){
    if(this.currentFacteur != this.facteursMultiplicatifs.length-1){
      this.currentFacteur +=1
    }else{
      this.currentFacteur = 0
    }
    this.worldService.facteurMultiplicatif = this.facteursMultiplicatifs[this.currentFacteur]
    this.worldService.facteurMultiplicatif$.next(this.worldService.facteurMultiplicatif);
  }

}
