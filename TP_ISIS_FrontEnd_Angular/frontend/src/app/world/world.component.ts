import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { WorldService } from '../Services/world.service';
import { Subscription, Subject } from 'rxjs';
import { ProgressBarComponent } from 'angular-progress-bar';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-world',
  templateUrl: './world.component.html',
  styleUrls: ['./world.component.css']
})
export class WorldComponent implements OnInit {

  worldSub:Subscription;
  world:any;

  notificationSub:Subscription;
  managerSub:Subscription;

  facteurMultiplicatifSub:Subscription;
  facteurMultiplicatif:number = 1;

  managerCango$ = new Subject<number>();

  products:any;

  firstPassageCount = 0;

  firstSub:Boolean = false;

  viewChecked:boolean = false;

  activeangels: number;

  @ViewChildren(ProgressBarComponent) progressBars;


  constructor(private worldService:WorldService, private notifService:ToastrService) { 
  }

  ngOnInit() {
    this.worldSub = this.worldService.world$.subscribe(
      (res)=>{
        this.world = res;
        this.products = this.world.products.product;
      }
    )
    this.notificationSub = this.worldService.notification$.subscribe(
      (res)=> {
        this.notifService.success(res[1], res[0]);
      }
    )
    this.managerSub = this.worldService.managerBuy$.subscribe(
      (res)=>{
        if(this.products[res-1].timeleft==0){
          this.animateProgressBar(this.products[res-1].id,true);
        }else{
          this.checkUpdateManager()
        }
      }
    )
    this.facteurMultiplicatifSub = this.worldService.facteurMultiplicatif$.subscribe(
      (res)=>{
        this.facteurMultiplicatif = res;
      }
    )
    this.worldService.getWorld()
  }

  ngAfterViewChecked(){
    if(this.products){
      var _this = this
      this.checkNextSeuil()
      if(this.viewChecked == false){
        this.viewChecked = true
        this.products.forEach(element => {
          if(element.managerUnlocked){
            this.firstPassageCount+=1;
            _this.animateProgressBar(element.id,true);
          }else{
            if(element.timeleft != 0){
              _this.animateProgressBar(element.id,false);
            }
          }
        });
      }
    } 
  }

  checkUpdateManager() {
    this.managerCango$.subscribe(
      (res) => {
        if(!this.firstSub){
          this.animateProgressBar(this.products[res].id, true)
          this.firstSub = true
        }else{
          this.firstSub = false
        }
      }
    )
  }

  startFabrication(id:number){
    this.worldService.prodProduct(id);
    this.animateProgressBar(id,false);
  }

  animateProgressBar(id:number,manager:boolean){
    let span_timeleft = document.getElementById("timeleft-"+id)
    let img_button = document.getElementById("img_button-"+id)
    
    let product_vitesse = this.products[id-1].vitesse;

    let timeleft;
    if(manager && this.products[id-1].timeleft!=0 && this.firstPassageCount !=0){
      timeleft = this.products[id-1].timeleft
      this.firstPassageCount -=1;
      this.progressBars._results[id-1].progress = ((product_vitesse-timeleft)/product_vitesse)*100;
    }else{
      if(manager){
        timeleft = product_vitesse
      }else{
        if(this.products[id-1].timeleft == 0){
          timeleft = product_vitesse
          this.products[id-1].timeleft = product_vitesse;
        }else{
          timeleft = this.products[id-1].timeleft
          this.progressBars._results[id-1].progress = ((product_vitesse-timeleft)/product_vitesse)*100;
        }
      }
    }

    let int = setInterval(increaseProgress, 10);

    var _this = this;
    function increaseProgress() {

      img_button.style.pointerEvents= "none";
      if(timeleft>0){
        timeleft = timeleft - 10
        let concatTimeleft = Math.round(timeleft/1000)
        span_timeleft.innerHTML = '<i class="ni ni-time-alarm mr-2"></i>' + concatTimeleft.toString() + ' secondes'
        _this.progressBars._results[id-1].progress = _this.progressBars._results[id-1].progress + ((10*100)/product_vitesse);
      }else{
        clearInterval(int);
        _this.progressBars._results[id-1].progress = 0;
        timeleft = 0
        let concatTimeleft_end = Math.round(timeleft/1000)
        span_timeleft.innerHTML = '<i class="ni ni-time-alarm mr-2"></i>'+ concatTimeleft_end.toString() + ' secondes'
        img_button.style.pointerEvents= "all";

        if(_this.worldService.world.activeangels > 0){
          _this.worldService.world.score += (_this.worldService.world.products.product[id-1].revenu*_this.worldService.world.products.product[id-1].quantite)
                                          * (1 + (_this.worldService.world.activeangels * _this.worldService.world.angelbonus) / 100)
          _this.worldService.world.totalangels = Math.floor(_this.worldService.world.score/300)

          if(!manager){
            _this.worldService.world.money += (_this.worldService.world.products.product[id-1].revenu*_this.worldService.world.products.product[id-1].quantite)
                                            * (1 + (_this.worldService.world.activeangels * _this.worldService.world.angelbonus) / 100)
            _this.products[id-1].timeleft = 0
            _this.worldService.emitWorld()
            if(_this.products[id-1].managerUnlocked){
              _this.managerCango$.next(id-1)
            }
          }else{
            _this.worldService.world.money += (_this.worldService.world.products.product[id-1].revenu*_this.worldService.world.products.product[id-1].quantite)
                                            * (1 + (_this.worldService.world.activeangels * _this.worldService.world.angelbonus) / 100)
            _this.worldService.emitWorld()
            _this.animateProgressBar(id,true)
          }
                                  
        }else{
          _this.worldService.world.score += _this.worldService.world.products.product[id-1].revenu*_this.worldService.world.products.product[id-1].quantite
          _this.worldService.world.totalangels = Math.floor(_this.worldService.world.score/300)

          if(!manager){
            _this.worldService.world.money += _this.worldService.world.products.product[id-1].revenu*_this.worldService.world.products.product[id-1].quantite
            _this.products[id-1].timeleft = 0
            _this.worldService.emitWorld()
            if(_this.products[id-1].managerUnlocked){
              _this.managerCango$.next(id-1)
            }
          }else{
            _this.worldService.world.money += _this.worldService.world.products.product[id-1].revenu*_this.worldService.world.products.product[id-1].quantite
            _this.worldService.emitWorld()
            _this.animateProgressBar(id,true)
          }
        }
      }
    }
    
  }


  nextSeuil(id:number){
      this.worldService.nextSeuil(id,this.calculCout(id))
  }

  checkNextSeuil(){
    for (let index = 0; index < this.products.length; index++) {
      let element = <HTMLInputElement> document.getElementById("button-"+this.products[index].id)
      let element2 = <HTMLInputElement> document.getElementById("btn-"+this.products[index].id)
      if(this.world.money >= this.calculCout(this.products[index].id)){
        element.disabled = false
        if(element2!=null){
          element2.disabled = false
        }
      }else{
        element.disabled = true
        if(element2!=null){
          element2.disabled = true
        }
      }
    }
  }

  calculCout(id:number){
    let cout = 0
    if(this.products[id-1].quantite != 0){
      if(this.facteurMultiplicatif == 1){
        return this.products[id-1].cout*Math.pow(this.products[id-1].croissance,this.products[id-1].quantite-1) 
      }else{
        for (let index = 1; index <= this.facteurMultiplicatif; index++) {
          cout = cout + (this.products[id-1].cout*Math.pow(this.products[id-1].croissance,this.products[id-1].quantite-1+index))
        }
        return cout
      }
    }else{
      if(this.facteurMultiplicatif == 1){
        return this.products[id-1].cout*Math.pow(this.products[id-1].croissance,this.products[id-1].quantite) 
      }else{
        for (let index = 1; index <= this.facteurMultiplicatif; index++) {
          cout = cout + (this.products[id-1].cout*Math.pow(this.products[id-1].croissance,this.products[id-1].quantite+index))
        }
        return cout
      }
    }


  }

}
