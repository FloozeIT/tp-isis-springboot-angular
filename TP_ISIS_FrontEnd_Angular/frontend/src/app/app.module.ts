import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { WorldComponent } from './world/world.component';
import { WorldService } from './Services/world.service';
import { HeaderComponent } from './header/header.component';

import {ProgressBarModule} from "angular-progress-bar";
import { ChangeUsernameComponent } from './change-username/change-username.component';
import { ManagersComponent } from './managers/managers.component'
import { ToastrModule } from 'ngx-toastr';
import { UnlocksComponent } from './unlocks/unlocks.component';
import { CashUpgradesComponent } from './cash-upgrades/cash-upgrades.component';
import { LoaderComponent } from './loader/loader.component';
import { InvestorsComponent } from './investors/investors.component';
 

@NgModule({
  declarations: [
    AppComponent,
    WorldComponent,
    HeaderComponent,
    ChangeUsernameComponent,
    ManagersComponent,
    UnlocksComponent,
    CashUpgradesComponent,
    LoaderComponent,
    InvestorsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ProgressBarModule,
    FormsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      closeButton:true,
      progressBar:true
      
    })
  ],
  providers: [WorldService],
  bootstrap: [AppComponent]
})
export class AppModule { }
