import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { WorldService } from './Services/world.service';
import { transition, style, animate, trigger, state } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations:[
    trigger('myAwesomeAnimation', [
      state('true', style({ opacity: 1, display:'block' })),
      state('false', style({ opacity: 0, display:'none' })),
      transition('true => false', animate(500))
    ]),
    trigger('enterAnimation', [
        transition(':enter', [
          style({opacity: 0}),
          animate('150ms', style({opacity: 1}))
        ]),
        transition(':leave', [
          style({opacity: 1}),
          animate('150ms', style({opacity: 0}))
        ])
    ])
  ]
})
export class AppComponent implements OnInit{
  menu:string = ""
  changeUsername:Boolean = false;

  loadingSub:Subscription
  loading:boolean = true;

  constructor(private notifService:ToastrService, private worldService:WorldService){}

  ngOnInit(){
    if(localStorage.getItem("username")==null){
        this.changeUsername = true
        this.notifService.success("Renseigner un pseudo", "Identification");
    }
    this.loadingSub = this.worldService.loading$.subscribe(
      (res)=>{
        this.loading = res
      }
    )

  }

  changeMenu(event){
    console.log(event)
    if(this.menu != event){
      this.menu = event
    }else{
      this.menu = "";
    }
  }

  checkUsername(){
    if(localStorage.getItem("username")==null || localStorage.getItem("username")=="Changervotrepseudo" || localStorage.getItem("username")==""){
      this.loading = false
      return true
    }else{
      return false
    }
  }

  changeStateUsername(event){
    if(event=="" || event=="Changer votre pseudo"){
       this.changeUsername = true
       this.notifService.success("Renseigner un pseudo", "Identification");
    }
  }
}
