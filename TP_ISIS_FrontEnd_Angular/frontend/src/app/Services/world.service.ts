import { Injectable } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class WorldService {

  constructor(private http:HttpClient, private toastr:ToastrModule) { }

  world : any;
  world$ = new Subject<[]>();
  facteurMultiplicatif = 1
  facteurMultiplicatif$ = new Subject<number>();

  notification:[string,string] = ["",""];  
  notification$ = new Subject<[string,string]>();
  notificationUnique:boolean = false

  managerBuy$ = new Subject<number>();
  username$ = new Subject<string>();

  loading$ = new Subject<boolean>();

  emitWorld(){
    console.log("emit world")
    this.world$.next(this.world);
  }

  emitNotification(){
    this.notification$.next(this.notification);
  }

  emitManagerBuy(id:number){
    console.log("emit manager")
    this.managerBuy$.next(id)
  }

  emitLoading(state:boolean){
    console.log("emit load")
    this.loading$.next(state)
  }

  emitUsername(user:string){
    this.username$.next(user)
  }

  getWorld(){
    this.emitLoading(true)
    let httpOptions = {
      headers: new HttpHeaders({
        'user':  localStorage.getItem("username"),
      })
    }

    console.log(localStorage.getItem("username"))

    this.http.get("http://localhost:9091/world", httpOptions).subscribe(
      (res)=>{
        this.world = res;
        this.emitWorld()
        setTimeout(() => {
          this.emitLoading(false)
        }, 1500);
      },(err)=>{
        console.log(err)
      }
    )
  }

  prodProduct(id:number){
    let httpOptions = {
      headers: new HttpHeaders({
        'user':  localStorage.getItem("username"),
      })
    }
    this.http.put("http://localhost:9091/produit", this.world.products.product[id-1], httpOptions).subscribe()
  }

  nextSeuil(id:number, cout:number){
    let httpOptions = {
      headers: new HttpHeaders({
        'user':  localStorage.getItem("username"),
      })
    }
    let product = this.world.products.product[id-1]
    this.world.money -= cout
    product.quantite += this.facteurMultiplicatif
    this.upgradePaliers(id);
    this.http.put("http://localhost:9091/produit", product, httpOptions).subscribe()
    this.emitWorld();
  }

  upgradePaliers(id:number){
    var _this = this;
    let product = this.world.products.product[id-1]
    product.palliers.pallier.forEach(element => {
      if(product.quantite >= element.seuil && element.unlocked==false){
        if(element.typeratio==="GAIN"){
          product.revenu = product.revenu*element.ratio
          product.logo = element.logo
          element.unlocked = true;
          _this.notification[0] = "Pallier "+ element.seuil + " atteint" 
          _this.notification[1] = "Bonus : "+element.typeratio+" pour "+ product.name ;
          _this.emitNotification();
        }else if(element.typeratio === "VITESSE"){
          product.vitesse = Math.round(product.vitesse / element.ratio)
          product.logo = element.logo
          element.unlocked = true;
          _this.notification[0] = "Pallier "+ element.seuil + " atteint" 
          _this.notification[1] = "Bonus : "+element.typeratio+" pour "+ product.name ;
          _this.emitNotification();
        }
      }
    });
    this.world.allunlocks.pallier.forEach(unlockPallier => {
      let compteur = 0;
      this.world.products.product.forEach(prod => {
        if(prod.quantite >= unlockPallier.seuil && unlockPallier.unlocked==false){
          compteur +=1
        }
      });
      if(compteur == this.world.products.product.length){
        if(unlockPallier.typeratio==="GAIN"){
          this.world.products.product.forEach(prod => {
            prod.revenu = prod.revenu*unlockPallier.ratio
          });
          unlockPallier.unlocked = true;
          _this.notification[0] = "Pallier "+ unlockPallier.seuil + " atteint" 
          _this.notification[1] = "Bonus : "+unlockPallier.typeratio+" pour tous les produits" ;
          _this.emitNotification();
        }else if(unlockPallier.typeratio === "VITESSE"){
          this.world.products.product.forEach(prod => {
            prod.vitesse = Math.round(prod.vitesse / unlockPallier.ratio)
          });
          unlockPallier.unlocked = true;
          _this.notification[0] = "Pallier "+ unlockPallier.seuil + " atteint" 
          _this.notification[1] = "Bonus : "+unlockPallier.typeratio+" pour tous les produits" ;
          _this.emitNotification();
        }
      }
    });
    console.log(this.world)
  }

  buyManager(idcible:number){
    let httpOptions = {
      headers: new HttpHeaders({
        'user':  localStorage.getItem("username"),
      })
    }

    this.world.managers.pallier[idcible-1].unlocked = true;
    this.world.products.product[idcible-1].managerUnlocked = true;
    this.world.money -= this.world.managers.pallier[idcible-1].seuil;

    this.http.put("http://localhost:9091/manager", this.world.managers.pallier[idcible-1], httpOptions).subscribe()
    this.emitManagerBuy(idcible);
    this.emitWorld();
  }

  buyUpgrade(idcible:number){
    let httpOptions = {
      headers: new HttpHeaders({
        'user':  localStorage.getItem("username"),
      })
    }

    this.world.upgrades.pallier[idcible-1].unlocked = true;
    this.world.products.product[idcible-1].revenu *= this.world.upgrades.pallier[idcible-1].ratio
    this.world.money -= this.world.upgrades.pallier[idcible-1].seuil;

    this.http.put("http://localhost:9091/upgrades", this.world.upgrades.pallier[idcible-1], httpOptions).subscribe()
    this.emitWorld();
  }

  resetWorld(){
    let httpOptions = {
      headers: new HttpHeaders({
        'user':  localStorage.getItem("username"),
      })
    }

    this.http.delete("http://localhost:9091/world",httpOptions).subscribe()
  }
}
