import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { WorldService } from '../Services/world.service';
import { Subscription } from 'rxjs';
import { NgOnChangesFeature } from '@angular/core/src/render3';

@Component({
  selector: 'app-investors',
  templateUrl: './investors.component.html',
  styleUrls: ['./investors.component.css']
})
export class InvestorsComponent implements OnInit {

  world:any;
  worldSub:Subscription;
  totalangels: any;
  activeangels: any;
  score: any;

  angelsclaimed: any;
  hasBeenReset: boolean;

  @Output() closeEvent = new EventEmitter<string>();

  constructor(private worldService:WorldService) { }

  ngOnInit() {
    this.worldSub = this.worldService.world$.subscribe(
      (res:any)=>{
        this.world = res;
        this.totalangels = res.totalangels;
        this.activeangels = res.activeangels;
        this.score = res.score;
      }
    )

    this.angelsclaimed = 0;
    this.worldService.emitWorld();    

    console.log("aa : " + this.activeangels)
    if(this.activeangels > 0){
      this.hasBeenReset = true;
    }else{
      this.hasBeenReset = false;
    }
  }

  updateAngelsClaimedToRestart(){
    if (!this.hasBeenReset){
      this.angelsclaimed = Math.floor(15 * Math.sqrt(this.world.score/Math.pow(10, 2)) - this.totalangels)
    }else{
      this.angelsclaimed = Math.floor(15 * Math.sqrt(this.world.score/Math.pow(10, 2)) - this.totalangels) - this.activeangels
    }
    return this.angelsclaimed
  }

  availabilityReset(){
    if(this.angelsclaimed > 0){
      return true;
    }else{
      return false;
    }
  }

  resetWorld(){   
    this.worldService.resetWorld();
    console.log(this.hasBeenReset)
    location.reload()
  }

}
