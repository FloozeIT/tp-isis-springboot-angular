import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { WorldService } from '../Services/world.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-managers',
  templateUrl: './managers.component.html',
  styleUrls: ['./managers.component.css']
})
export class ManagersComponent implements OnInit {

  world:any;
  managers:any;
  worldSub:Subscription;

  @Output() closeEvent = new EventEmitter<string>();

  constructor(private worldService:WorldService) { }

  ngOnInit() {
    this.worldSub = this.worldService.world$.subscribe(
      (res:any)=>{
        this.world = res;
        this.managers = res.managers.pallier;
      }
    )
    this.worldService.emitWorld();
  }

  availabilityManager(id:number){
    if(this.world.money>=this.managers[id-1].seuil){
      return true;
    }else{
      return false;
    }
  }

  buyManager(idproduct:number){
    this.worldService.buyManager(idproduct);
  }

}
