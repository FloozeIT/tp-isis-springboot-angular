import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { WorldService } from '../Services/world.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-unlocks',
  templateUrl: './unlocks.component.html',
  styleUrls: ['./unlocks.component.css']
})
export class UnlocksComponent implements OnInit {

  @Output() closeEvent = new EventEmitter<string>();
  tab:string = 'AllUnlocks';

  worldSub:Subscription
  products:any;
  allUnlocks:any;
  palliersPerso = [];

  constructor(private worldService:WorldService) { }

  ngOnInit() {
    this.worldSub = this.worldService.world$.subscribe(
      (res:any)=>{
        this.products = res.products.product;
        this.allUnlocks = res.allunlocks
	this.palliersPerso = []
        this.products.forEach(product => {
          product.palliers.pallier.forEach(pallier => {
            this.palliersPerso.push(pallier)
          });
        });
        console.log(this.palliersPerso)
      }
    )
    this.worldService.emitWorld();
  }

}
