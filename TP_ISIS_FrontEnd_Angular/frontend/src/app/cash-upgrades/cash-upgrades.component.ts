import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { WorldService } from '../Services/world.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cash-upgrades',
  templateUrl: './cash-upgrades.component.html',
  styleUrls: ['./cash-upgrades.component.css']
})
export class CashUpgradesComponent implements OnInit {

  world:any;
  upgrades:any;
  worldSub:Subscription;

  @Output() closeEvent = new EventEmitter<string>();

  constructor(private worldService:WorldService) { }

  ngOnInit() {
    this.worldSub = this.worldService.world$.subscribe(
      (res:any)=>{
        this.world = res;
        this.upgrades = res.upgrades.pallier;
      }
    )
    this.worldService.emitWorld();
  }

  availabilityUpgrades(id:number){
    if(this.world.money>=this.upgrades[id-1].seuil){
      return true;
    }else{
      return false;
    }
  }

  buyUpgrade(idproduct:number){
    this.worldService.buyUpgrade(idproduct);
  }
}
