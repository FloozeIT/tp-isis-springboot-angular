import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { WorldService } from '../Services/world.service';

@Component({
  selector: 'app-change-username',
  templateUrl: './change-username.component.html',
  styleUrls: ['./change-username.component.scss']
})
export class ChangeUsernameComponent implements OnInit {

  @Output() close = new EventEmitter<string>();

  username:string;

  constructor(private worldService:WorldService) { }

  ngOnInit() {
    if(localStorage.getItem("username")){
      this.username = localStorage.getItem("username")
    }else{
      this.username = "Changer votre pseudo"
    }

  }

  onEnter(value:string){
    value = value.replace(/\s/g, '');
    if(value == ""){
      
    }else{
      localStorage.setItem("username",value);
      this.worldService.emitUsername(value)
      //this.close.emit(value);
      //this.worldService.getWorld()
      location.reload()
    }
  }

  saveChanges(){
    this.username = this.username.replace(/\s/g, '');
    if(this.username == "" || this.username == "Changervotrepseudo"){
      this.close.emit("Changer votre pseudo");
    }else{
      localStorage.setItem("username",this.username);
      this.close.emit(this.username);
      this.worldService.getWorld()
    }
  }

  closeModal(){
    if(localStorage.getItem("username")){
      this.close.emit(localStorage.getItem("username"))
    }else{
      this.close.emit("Changer votre pseudo")
    }
    
  }

}
