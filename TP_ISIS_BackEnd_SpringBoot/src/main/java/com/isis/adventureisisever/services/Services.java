package com.isis.adventureisisever.services;



import com.google.gson.Gson;
import com.isis.adventureisisever.models.World;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONObject;

import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.crypto.Data;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class Services {
    public World readWorldFromXml(String username){
        try{
            File world_xml = new File("world_"+username+".xml");
            if(!world_xml.exists()){
                System.out.println("enter not exist");
                try{
                    InputStream input_base = getClass().getClassLoader().getResourceAsStream("world.xml");
                    JAXBContext cont = JAXBContext.newInstance(World.class);
                    Unmarshaller u = cont.createUnmarshaller();
                    World world_base = (World) u.unmarshal(input_base);

                    Marshaller m = cont.createMarshaller();
                    m.marshal(world_base, new File("world_"+username+".xml"));
                }catch (Exception e){
                    return null;
                }
                try{
                    File world_xml_new = new File("world_"+username+".xml");
                    JAXBContext cont_new = JAXBContext.newInstance(World.class);
                    Unmarshaller u_new = cont_new.createUnmarshaller();
                    World world_new = (World) u_new.unmarshal(world_xml_new);
                    return world_new;
                }catch (Exception e){
                    return null;
                }
            }else{
                System.out.println("enter exist");
                JAXBContext cont = JAXBContext.newInstance(World.class);
                Unmarshaller u = cont.createUnmarshaller();
                World world = (World) u.unmarshal(world_xml);
                return world;
            }
        }
        catch (Exception e){
            return null;
        }
    }

    public World resetWorldToXml(String username, World previous_world){
        try{
            InputStream input_base = getClass().getClassLoader().getResourceAsStream("world.xml");
            JAXBContext cont = JAXBContext.newInstance(World.class);
            Unmarshaller u = cont.createUnmarshaller();
            World new_world = (World) u.unmarshal(input_base);
            new_world.setScore(previous_world.getScore());
            new_world.setTotalangels(previous_world.getTotalangels());
            new_world.setActiveangels(previous_world.getActiveangels() + Math.round(15 * Math.sqrt(previous_world.getScore()/Math.pow(10, 2)) - previous_world.getTotalangels()));
            System.out.println(previous_world.getScore());

            Marshaller m = cont.createMarshaller();
            m.marshal(new_world, new File("world_"+username+".xml"));

            return new_world;
        }catch (Exception e){
            return null;
        }

    }

    public void saveWorldToXml(String username, World world){
        try{
            JAXBContext cont = JAXBContext.newInstance(World.class);
            Marshaller m = cont.createMarshaller();
            m.marshal(world, new File("world_"+username+".xml"));
        }catch (Exception e){}
    }
}
