package com.isis.adventureisisever.controller;


import com.google.gson.Gson;
import com.isis.adventureisisever.models.PallierType;
import com.isis.adventureisisever.models.ProductType;
import com.isis.adventureisisever.models.ProductsType;
import com.isis.adventureisisever.models.World;
import com.isis.adventureisisever.services.Services;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Timer;
import java.util.TimerTask;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/")
public class Worldcontroller {

	Services services;

	public Worldcontroller(){
		services = new Services();
	}


	@RequestMapping(value="/world",method=RequestMethod.GET,produces="application/json")
	public World getWorld(HttpServletRequest request){
		String username = request.getHeader("user");
		World world =  services.readWorldFromXml(username);
		if(world.getLastupdate() == 0){
			return world;
		}else{
			World world_updated = upgradeSinceLastUpdate(username,world);
			return world_updated;
		}
	}

    @RequestMapping(value="/produit",method=RequestMethod.PUT,produces="application/json")
    public Boolean updateProduct(HttpServletRequest request,@RequestBody String produit){
		String username = request.getHeader("user");
		ProductType produit_map = new Gson().fromJson(produit, ProductType.class);

		World world =  services.readWorldFromXml(username);
		World world_updated = upgradeSinceLastUpdate(username,world);

        ProductType produit_toUpdate = world_updated.getProducts().getProduct().get(produit_map.getId()-1);

        if (produit_toUpdate == null) { return false;}

		int qtchange = produit_map.getQuantite() - produit_toUpdate.getQuantite();
        if(qtchange>0){
			System.out.println("quantitytytytyt = "+produit_map.getQuantite());
			double cout = 0;
			double total;
			if(qtchange == 1){
				System.out.println("testde"+produit_toUpdate.getQuantite());
				if(produit_toUpdate.getQuantite()==0){
					total = world_updated.getMoney() - produit_toUpdate.getCout()*Math.pow(produit_toUpdate.getCroissance(),produit_toUpdate.getQuantite());
				}else{
					total = world_updated.getMoney() - produit_toUpdate.getCout()*Math.pow(produit_toUpdate.getCroissance(),produit_toUpdate.getQuantite()-1);
				}
			}else{
				for (Integer index = 1; index <= qtchange; index++) {
					cout = cout + (produit_toUpdate.getCout()*Math.pow(produit_toUpdate.getCroissance(),produit_toUpdate.getQuantite()-1+index));
					System.out.println(cout);
				}
				total = world_updated.getMoney() - cout;
			}
			produit_toUpdate.setQuantite(produit_toUpdate.getQuantite()+qtchange);
			upgradePallier(world_updated, produit_toUpdate);
			//produit_toUpdate.setRevenu(produit_toUpdate.getRevenu()*produit_toUpdate.getCroissance());
			//produit_toUpdate.setCout(produit_toUpdate.getCout()*produit_toUpdate.getCroissance());
			world_updated.setMoney(total);
			services.saveWorldToXml(username, world_updated);
		}else{
        	if(produit_map.isManagerUnlocked()){
				//loopManager(username, produit_map, world);
				//world.getManagers().getPallier().get(produit_toUpdate.getId()-1).setUnlocked(true);
				//world.getProducts().getProduct().get(produit_toUpdate.getId()-1).setManagerUnlocked(true);
				//world.setLastupdate(System.currentTimeMillis());
			}else{
				produit_toUpdate.setTimeleft(produit_toUpdate.getVitesse());
        		//loopManager(username,produit_toUpdate);
				//world.setMoney(world.getMoney()+produit_map.getRevenu());
			}
		}
        services.saveWorldToXml(username, world_updated);
        return true;
    }

	@RequestMapping(value="/manager",method=RequestMethod.PUT,produces="application/json")
	public void setManager(HttpServletRequest request, @RequestBody String manager){
		String username = request.getHeader("user");
		World world =  services.readWorldFromXml(username);
		World world_updated = upgradeSinceLastUpdate(username,world);
		PallierType manager_map = new Gson().fromJson(manager, PallierType.class);
		world_updated.setMoney(world_updated.getMoney()-manager_map.getSeuil());
		world_updated.getManagers().getPallier().get(manager_map.getIdcible()-1).setUnlocked(true);
		world_updated.getProducts().getProduct().get(manager_map.getIdcible()-1).setManagerUnlocked(true);
		world_updated.setLastupdate(System.currentTimeMillis());
		services.saveWorldToXml(username,world_updated);
	}

	@RequestMapping(value="/upgrades",method=RequestMethod.PUT,produces="application/json")
	public void setUpgrade(HttpServletRequest request, @RequestBody String upgrade){
		String username = request.getHeader("user");
		World world =  services.readWorldFromXml(username);
		World world_updated = upgradeSinceLastUpdate(username,world);
		PallierType upgrade_map = new Gson().fromJson(upgrade, PallierType.class);
		world_updated.setMoney(world_updated.getMoney()-upgrade_map.getSeuil());
		world_updated.getUpgrades().getPallier().get(upgrade_map.getIdcible()-1).setUnlocked(true);
		world_updated.getProducts().getProduct().get(upgrade_map.getIdcible()-1).setRevenu(world_updated.getProducts().getProduct().get(upgrade_map.getIdcible()-1).getRevenu()*world_updated.getUpgrades().getPallier().get(upgrade_map.getIdcible()-1).getRatio());
		world_updated.setLastupdate(System.currentTimeMillis());
		services.saveWorldToXml(username,world_updated);
	}

	@RequestMapping(value="/world",method=RequestMethod.DELETE,produces="application/json")
	public void resetWorld(HttpServletRequest request){
		System.out.println("reset world");
		String username = request.getHeader("user");
		World previous_world = getWorld(request);
		System.out.println(previous_world.getScore());
		services.resetWorldToXml(username, previous_world);
	}

	private void upgradePallier(World world, ProductType produit){
		for (PallierType element : produit.getPalliers().getPallier()) {
			if(produit.getQuantite() >= element.getSeuil() && element.isUnlocked()==false){
				if(element.getTyperatio().toString()=="GAIN"){
					produit.setRevenu(produit.getRevenu()*element.getRatio());
					produit.setLogo(element.getLogo());
					element.setUnlocked(true);
				}else if(element.getTyperatio().toString() == "VITESSE"){
					produit.setVitesse((int) (produit.getVitesse() / element.getRatio()));
					produit.setLogo(element.getLogo());
					element.setUnlocked(true);
				}
			}
		}
		for (PallierType unlockPallier : world.getAllunlocks().getPallier()) {
			Integer compteur = 0;
			for (ProductType prod : world.getProducts().getProduct()) {
				if (prod.getQuantite() >= unlockPallier.getSeuil() && unlockPallier.isUnlocked() == false) {
					compteur += 1;
				}
			}
			if (compteur == world.getProducts().getProduct().size()) {
				if (unlockPallier.getTyperatio().toString() == "GAIN") {
					for (ProductType prod : world.getProducts().getProduct()) {
						prod.setRevenu(prod.getRevenu() * unlockPallier.getRatio());
					}
					unlockPallier.setUnlocked(true);
				} else if (unlockPallier.getTyperatio().toString() == "VITESSE") {
					for (ProductType prod : world.getProducts().getProduct()) {
						prod.setVitesse((int) (prod.getVitesse() / unlockPallier.getRatio()));
					}
					unlockPallier.setUnlocked(true);
				}
			}
		}
	}

	private World upgradeSinceLastUpdate(String username, World world){
		double time_since_last_update = System.currentTimeMillis() - world.getLastupdate();
		if (world.getActiveangels() > 0) {
			double bonus;
			for (ProductType element: world.getProducts().getProduct()) {
				if(element.isManagerUnlocked()){
					int quantity_producted;
					int timeleft_product;
					if(time_since_last_update>element.getVitesse()){
						int time_since_moins_timeleft = (int) (time_since_last_update-element.getTimeleft());
						quantity_producted = Math.round(time_since_moins_timeleft/element.getVitesse());
						if(element.getTimeleft()!=0){
							quantity_producted +=1;
						}
						timeleft_product = element.getVitesse() - Math.round(time_since_moins_timeleft%element.getVitesse());
					}else{
						if(element.getTimeleft() == 0){
							timeleft_product = (int) (element.getVitesse()-time_since_last_update);
							quantity_producted = 0;
						}else{
							timeleft_product = (int) (element.getTimeleft()-time_since_last_update);
							if(timeleft_product<=0){
								quantity_producted = 1;
								timeleft_product = element.getVitesse() - Math.abs(timeleft_product);
							}else{
								quantity_producted = 0;
							}
						}
					}
					bonus = 1 + (world.getActiveangels() * world.getAngelbonus() / 100);
					world.setMoney(world.getMoney()+((element.getRevenu()*element.getQuantite())*(quantity_producted)*bonus));
					world.getProducts().getProduct().get(element.getId()-1).setTimeleft(timeleft_product);
					world.setScore((world.getScore()+((element.getRevenu()*element.getQuantite())*(quantity_producted)*bonus)));
				}else{
					if(element.getTimeleft()>0){
						int quantity_producted;
						int timeleft_product;
						if(time_since_last_update>=element.getVitesse()){
							System.out.println("pass sup");
							quantity_producted = 1;
							timeleft_product = 0;
						}else{
							if(time_since_last_update>element.getTimeleft()){
								System.out.println("pass inf + quantity");
								quantity_producted = 1;
								timeleft_product = 0;
							}else{
								System.out.println("pass inf");
								quantity_producted = 0;
								timeleft_product = (int) (element.getTimeleft() - time_since_last_update);
							}
						}
						bonus = 1 + (world.getActiveangels() * world.getAngelbonus() / 100);
						world.setMoney(world.getMoney()+((element.getRevenu()*element.getQuantite())*(quantity_producted)*bonus));
						world.getProducts().getProduct().get(element.getId()-1).setTimeleft(timeleft_product);
						world.setScore((world.getScore()+((element.getRevenu()*element.getQuantite())*(quantity_producted)*bonus)));
					}
				}
			}
		}else{
			for (ProductType element: world.getProducts().getProduct()) {
				if(element.isManagerUnlocked()){
					int quantity_producted;
					int timeleft_product;
					if(time_since_last_update>element.getVitesse()){
						int time_since_moins_timeleft = (int) (time_since_last_update-element.getTimeleft());
						quantity_producted = Math.round(time_since_moins_timeleft/element.getVitesse());
						if(element.getTimeleft()!=0){
							quantity_producted +=1;
						}
						timeleft_product = element.getVitesse() - Math.round(time_since_moins_timeleft%element.getVitesse());
					}else{
						if(element.getTimeleft() == 0){
							timeleft_product = (int) (element.getVitesse()-time_since_last_update);
							quantity_producted = 0;
						}else{
							timeleft_product = (int) (element.getTimeleft()-time_since_last_update);
							if(timeleft_product<=0){
								quantity_producted = 1;
								timeleft_product = element.getVitesse() - Math.abs(timeleft_product);
							}else{
								quantity_producted = 0;
							}
						}
					}
					world.setMoney(world.getMoney()+((element.getRevenu()*element.getQuantite())*(quantity_producted)));
					world.getProducts().getProduct().get(element.getId()-1).setTimeleft(timeleft_product);
					world.setScore((world.getScore()+((element.getRevenu()*element.getQuantite())*(quantity_producted))));
				}else{
					if(element.getTimeleft()>0){
						int quantity_producted;
						int timeleft_product;
						if(time_since_last_update>=element.getVitesse()){
							System.out.println("pass sup");
							quantity_producted = 1;
							timeleft_product = 0;
						}else{
							if(time_since_last_update>element.getTimeleft()){
								System.out.println("pass inf + quantity");
								quantity_producted = 1;
								timeleft_product = 0;
							}else{
								System.out.println("pass inf");
								quantity_producted = 0;
								timeleft_product = (int) (element.getTimeleft() - time_since_last_update);
							}
						}
						world.setMoney(world.getMoney()+((element.getRevenu()*element.getQuantite())*(quantity_producted)));
						world.getProducts().getProduct().get(element.getId()-1).setTimeleft(timeleft_product);
						world.setScore((world.getScore()+((element.getRevenu()*element.getQuantite())*(quantity_producted))));
					}
				}
			}
		}

		System.out.println("Score : " + world.getScore());

		world.setTotalangels(Math.floor(world.getScore()/300));

		world.setLastupdate(System.currentTimeMillis());
		services.saveWorldToXml(username, world);
		return world;
	}

	/*private void loopManager(String username, ProductType product){
		new Timer().scheduleAtFixedRate(new TimerTask(){
			Boolean firstPassage = true;
			World world = services.readWorldFromXml(username);
			@Override
			public void run(){
				World world_updated = upgradeSinceLastUpdate(username,world);
				ProductType produit_toUpdate = world_updated.getProducts().getProduct().get(product.getId()-1);
				if(firstPassage){
					produit_toUpdate.setTimeleft(produit_toUpdate.getVitesse());
				}
				this.firstPassage = false;
				System.out.println(produit_toUpdate.getTimeleft());
				if(produit_toUpdate.getTimeleft()>0){
					produit_toUpdate.setTimeleft(produit_toUpdate.getTimeleft()-100);
				}else{
					produit_toUpdate.setTimeleft(0);
					world_updated.setMoney(world_updated.getMoney()+produit_toUpdate.getRevenu());
					this.cancel();
				}
				services.saveWorldToXml(username,world_updated);
			}
		},0,100);
	}*/

}
